Bacula, network-based backup, recovery & verification system	
http://www.bacula.org
===============================================================================

The 'bacula-common' package provides some of the common infrastructure
for Bacula. You can find Bacula READMEs and other documentation files
there.

BASIC INFORMATION
-----------------

To use Bacula you must install one of the flavors of director,
depending on your preferred database, onto a server.

You must also install bacula-sd on the machine where you will be
storing your backed-up data. And you'll need bacula-fd on any machine
that you want to back up.

The installation will run the necessary daemons automatically, but to
get any backups working you must customize the relevant configuration
files in /etc/bacula as described in the Bacula manual (available in
the package bacula-doc). Once you have edited the files you should
issue an appropriate "service bacula-xx force-reload".

NETWORK ENABLING
----------------

By default, Debian's Bacula packages install config files in which the
servers listen only on localhost by default. If you want to be able
to do backups over the network, you'll need to find the lines in
bacula-dir.conf, bacula-sd.conf and bacula-fd.conf that reference
127.0.0.1 and remove them.

Note that all hostnames used in "Address"-lines need to be resolvable
on all hosts involved, so fully qualified domain names are
recommended.

Using a hostname in the listen addresses (DirAddress, SDAddress,
FDAddress) will only work if they are resolvable on startup, this is
not the case if using NetworkManager.

PACKAGES
========

The package 'bacula-director' provides the bacula-dir binary.

To select a database backend, one of the following packages
has to be installed:

	- bacula-director-sqlite3
	- bacula-director-mysql
	- bacula-director-pgsql

These packages provide the needed scripts and configurations for
the database indicated in the package's name.

CHANGING DATABASE BACKEND
=========================

This is NOT RECOMMENDED and NOT SUPPORTED FOR LIVE DATA.

Your current database IS NOT transferred to the new database
backend. The new database backend will have an EMPTY database. Your
/etc/bacula/bacula-dir.conf file WILL NOT be automatically adapted to
function with the new database backend, YOU need to do the necessary
changes to the Catalog{} section YOURSELF.

If you want to go ahead anyway, you change the database backend by
installing a different bacula-director-DBTYPE package. This will pull
in the needed dependencies for the new database backend and remove the
ones from the old one.

CONSOLE
=======

The package bacula-console provides the administration console for
Bacula, so that users can contact the director.

The BAT graphical admin console is contained in the
bacula-console-qt package.

USERS & SECURITY
================

The passwords in the configuration files in /etc/bacula have been
auto-generated individually during installation. They are saved in
/etc/bacula/common_default_passwords.

The installation will create a "bacula" user on the system, and the
daemons and RunBefore/RunAfter jobs will run as that user. However,
the file daemon runs as root by default, as will your
ClientRunBefore/After jobs. The installation of the director will ask
you for a database user to use.

You can run Bacula consoles as any user, but the user you use will
need to be able to read the console config files. These files are, by
default, under /etc/bacula. You'll need to specify the config file
with -c for any of the consoles.

RUNNING BACULA-FD AS UNPRIVILEGED USER ON LINUX
===============================================

When using systemd:
-------------------

bacula-fd can be run as unprivileged user under systemd using the
following steps:

- Use "systemctl edit bacula-fd.service" to create an override
  configuration file in /etc/systemd/system/bacula-fd.service.d/.

-  Put the following content into the override configuration:

# Run bacula-fd as unprivileged user
[Service]
User=bacula
Group=bacula
AmbientCapabilities=CAP_DAC_READ_SEARCH

- Restart bacula-fd via "systemctl restart bacula-fd.service"

Now the FD runs as unprivileged user "bacula" but the capabilities
provided allow it read-only access to any file and directory on
the system, just as if it where run as user "root".

When using sysvinit:
--------------------

- Edit the file /etc/defaults/bacula-fd and add the following lines:

# Run bacula-fd as unprivileged user
BUSER="bacula"
BGROUP="bacula"

- Restart bacula-fd via "service bacula-fd restart"

Now the FD runs as unprivileged user "bacula" but it retains the
read-only capability to any file and directory on the system,
just as if it where run as user "root".

Notes:
------

This setup has some caveats though:

- ClientScripts no longer run as "root". If you need those scripts
  to run as root you need to adapt them to use a mechanism to run
  them as a different user, for example via sudo.

- Restored files no longer have their original permissions and ACLs.
  This can be changed when using systemd by also specifying the
  capabilities "CAP_CHOWN CAP_DAC_OVERRIDE CAP_FOWNER" but this puts
  the running process at nearly the same level as running as root 
  would do to beging with.
  When using SysVinit, there is no way to allow a non-root FD to
  restore files with the original permissions and ACLs.

UPGRADES
========

Debian packages will, unless you have asked them not to, attempt to
automatically upgrade your catalog during a major Bacula upgrade.

However, please be sure to examine the Release Notes in
/usr/share/doc/bacula-common/NEWS.gz for potential changes in the
configuration files.

CATALOG BACKUPS
===============

Please see the comments in the bacula-dir.conf file regarding the
make_catalog_backup script for information on tailoring it for your own
particular environment.

MONITORING ("NAGIOS") PLUGIN
============================

The monitoring plugin "check_bacula" from the examples is included in
bacula-common. Please note that it does not support TLS and that is
not an official part of bacula and thus unsupported.

USING ON KFREEBSD
=================

For some notes specific to kFreeBSD see
/usr/share/doc/bacula-common/README.Debian-kFreeBSD.gz

FURTHER READING
===============

See the Bacula documentation provided in the package bacula-doc and on
the upstream author's website, http://www.bacula.org.

-- Jose Luis Tallon, updated by John Goerzen, Carsten Leonahrdt and
Sven Hartge
