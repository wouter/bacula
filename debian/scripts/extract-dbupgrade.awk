#!/usr/bin/awk -f

# extract database creation/upgrade statements from the input file

# usage: call with "awk -v version=<target version> -f <this file>
# if version is a number, it extracts the upgrade statements to upgrade *to* that version
# if version=="ignore", it ignores the version and extracts the first END-OF-DATA block

# used by debian/scripts/install-dbconfig-upgrade


# (C) 2016 Carsten Leonhardt <leo@debian.org>

# License: expat

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:

#     The above copyright notice and this permission notice shall be
#     included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


BEGIN {
    version_found = 0
    data_found = 0
    if ( version == "ignore" ) {
	# don't mind the version, just search for END-OF-DATA
	version_found = 1
    }
    else {
	# decrease version from target to source version
	version--
	if ( version < 1 ) {
	    print "error: please pass target version in variable 'version'"
	    exit 1
	}
    }
}

{
    # skip to version check for the desired version
    if ( !version_found && !data_found && ($0 ~ "DBVERSION.* +(-eq|=) " version " ") ) {
	version_found = 1
	next
    }
    # skip to beginning of data block (marked by "<<END-OF-DATA")
    if ( version_found && !data_found && ($0 ~ "<<END-OF-DATA$") ) {
	data_found = 1
	next
    }
    # exit if end of data block is reached
    if ( version_found && data_found && ($0 ~ "^END-OF-DATA$") )
	exit
    # skip "USE ${db_name};" line, found in mysql files but handled by dbconfig
    if ( $0 ~ "^USE \\${db_name};" )
	next
    # otherwise print the data line
    if ( version_found && data_found )
	print $0
}

END {
    # don't print more messages if no suitable version was passed
    if ( version < 1 ) {
	exit 1
    }
    if ( !version_found ) {
	print "error: target version not found"
	exit 1
    }
    if ( !data_found ) {
	print "error: data block not found for target version"
	exit 1
    }
}
